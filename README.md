# README #

### What is this for? ###

* Run checks before a commit to ensure a better quality
* Version 1.0

### How do I get set up? ###

* Checkout the repo and then add all the file into your `<project-name>/.git/hooks`
```bash
git checkout git@bitbucket.org:gdesilets/git_hooks.git
cd git_hooks
mv . <project-name>/.git/hooks/
```
_if you only want one hook, simply checkout rename the desired hook to pre-commit move the file into `<project-name>/.git/hooks`_

** Important ! you must make them as executable, you'll need to do `sudo chmod +x <filename>` **


### TODO ###

* Clean up the code
* make an executable installer

### Who do I talk to? ###

* Gabriel Desilets - gdesilets@cognibox.com

Any contribution is welcome ;)